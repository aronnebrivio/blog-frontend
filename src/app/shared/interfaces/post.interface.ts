import { User } from './user.interface';

export interface Post {
    id: number,
    user_id: number,
    title: string,
    text: string,
    comments_count: number,
    created_at: string,
    updated_at: string,
    user: User,
}
