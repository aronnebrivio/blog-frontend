import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Post } from '../../shared/interfaces/post.interface';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent {
  @Input() items: Post[]

  constructor(
    private router: Router,
  ) { }

  postClicked = (id: number) => this.router.navigate(['/post', id])
}
