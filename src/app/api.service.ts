import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Post } from './shared/interfaces/post.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private baseUrl: string

  constructor(
    private http: HttpClient
  ) {
    this.baseUrl = environment.apiUrl
  }

  getPosts = (): Observable<Post[]> => this.http.get<Post[]>(this.baseUrl + '/posts')
  getPost = (id: number): Observable<Post> => this.http.get<Post>(this.baseUrl + '/posts/' + id)

  getCommentsForPost = (postId: number): Observable<Comment[]> => this.http.get<Comment[]>(this.baseUrl + '/comments?post_id=' + postId)
}
