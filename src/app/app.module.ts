import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { ApiService } from './api.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { PostPageComponent } from './post/post-page/post-page.component';
import { PostModule } from './post/post.module';
import { PostsPageComponent } from './post/posts-page/posts-page.component';
import { SharedModule } from './shared/shared.module';

const appRoutes: Routes = [
  { path: 'posts', component: PostsPageComponent },
  { path: 'post/:id', component: PostPageComponent },
  {
    path: '',
    redirectTo: '/posts',
    pathMatch: 'full'
  },
  { path: '**', component: PostsPageComponent }
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        scrollPositionRestoration: 'enabled',
      }
    ),
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    PostModule,
    HttpClientModule,
    MaterialModule,
    SharedModule,
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
