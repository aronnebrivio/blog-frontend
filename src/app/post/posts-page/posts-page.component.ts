import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/api.service';
import { Post } from '../../shared/interfaces/post.interface';

@Component({
  selector: 'app-posts-page',
  templateUrl: './posts-page.component.html',
  styleUrls: ['./posts-page.component.scss']
})
export class PostsPageComponent implements OnInit {
  posts$: Observable<Post[]>

  constructor(
    private api: ApiService
  ) { }

  ngOnInit() {
    this.posts$ = this.api.getPosts()
  }
}
