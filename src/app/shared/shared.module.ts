import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from '../material.module';
import { CommentListItemComponent } from './components/comment-list-item/comment-list-item.component';
import { PostListItemComponent } from './components/post-list-item/post-list-item.component';

@NgModule({
    declarations: [
        PostListItemComponent,
        CommentListItemComponent,
    ],
    imports: [
        CommonModule,
        MaterialModule,
    ],
    exports: [
        PostListItemComponent,
        CommentListItemComponent,
    ]
})
export class SharedModule { }
