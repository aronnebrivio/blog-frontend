import { Component, Input } from '@angular/core';
import { Post } from '../../interfaces/post.interface';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent {
  @Input() post: Post

  get publisherName() {
    const user = this.post.user
    if (user.first_name && user.last_name)
      return user.first_name + ' ' + user.last_name
    return user.email
  }
}
