import { User } from './user.interface';

export interface Comment {
    id: number,
    text: string,
    created_at: string,
    updated_at: string,
    user: User,
}
